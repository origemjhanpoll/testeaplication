import React, { Component } from 'react';
import Router from './router'
import { View, Text } from 'react-native';


class App extends Component {
  render() {
    return (
      <Router />
    );
  }
}
export default App;
