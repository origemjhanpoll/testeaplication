import React from "react";
import { Router, Stack, Scene, Actions } from "react-native-router-flux";

import { login } from './login'

const loginRouter = () => {
  return (
    <Scene
      key='keyLogin'
      title='Login'
      component={login}
    />
  )
}

export default loginRouter;