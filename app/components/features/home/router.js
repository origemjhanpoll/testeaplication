import React from "react";
import { Router, Stack, Scene, Actions } from "react-native-router-flux";

import { Home } from './home'

const homeRouter = () => {
  return (
    <Stack key='homeStack' title='Home' >
      <Scene
        key='keyHome'
        title='Home'
        component={Home}
      />
    </Stack>
  )
}

export default homeRouter;