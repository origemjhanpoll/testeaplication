import React from "react";
import { Router, Stack, Scene, Actions } from "react-native-router-flux";

import loginRouter from './components/login/router'
import homeRouter from './components/features/home/router'
import settingRouter from './components/features/setting/router'

const RouterComponent = () => {
  return (
    <Router>
      <Stack>
        {loginRouter()}
        <Stack tabs showLabel={true}
          tabBarStyle={{ paddingBottom: 14 }}
          labelStyle={{ fontSize: 14 }}
        >
          {homeRouter()}
          {settingRouter()}
        </Stack>
      </Stack>
    </Router>
  )
}

export default RouterComponent;